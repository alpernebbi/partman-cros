# THIS PACKAGE IS PART OF THE L10N-SYNC PROCESS FOR debian-installer PACKAGES!
# DO NOT TRANSLATE THIS MATERIAL DIRECTLY, SUCH WORK WOULD BE LOST !!!
# IN DOUBT, ASK ON debian-boot@lists.debian.org

# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the partman-cros package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: partman-cros\n"
"Report-Msgid-Bugs-To: partman-cros@packages.debian.org\n"
"POT-Creation-Date: 2022-11-29 18:52+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: text
#. Description
#. :sl4:
#: ../partman-cros.templates:1001
msgid "ChromeOS kernel partition"
msgstr ""

#. Type: text
#. Description
#. Short form of ChromeOS/ChromiumOS. Keep short.
#. :sl4:
#: ../partman-cros.templates:2001
msgid "CrOS"
msgstr ""

#. Type: boolean
#. Description
#. Users are familiar with boot methods as necessary to boot d-i itself.
#. :sl4:
#: ../partman-cros.templates:3001
msgid "Go back to the menu and resume partitioning?"
msgstr ""

#. Type: boolean
#. Description
#. Users are familiar with boot methods as necessary to boot d-i itself.
#. :sl4:
#: ../partman-cros.templates:3001
msgid ""
"No usable ChromeOS kernel partition is found. At least one such partition is "
"necessary for your machine to be bootable with ChromeOS' verified boot "
"methods (with CTRL+D or CTRL+U). These partitions must reside on the same "
"physical disk as either the root or the boot partition. Two 512 MB "
"partitions should be fine for most installations."
msgstr ""

#. Type: boolean
#. Description
#. Users are familiar with boot methods as necessary to boot d-i itself.
#. :sl4:
#: ../partman-cros.templates:3001
msgid ""
"If you cannot boot into the installed system in another way (like legacy "
"boot with CTRL+L), continuing the installation would result in an unbootable "
"machine."
msgstr ""
